using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TripleInstashoot : Bullet {

	public int maxAmmo = 2;
	Cartridge cartridge;

	void Awake()
	{
		cartridge = GameObject.Find("Weapons").GetComponent<Weapons>().cartridges[0];
	}
	
	protected void Update()
	{
	     if (shooting) {
			transform.Translate (0, speed * Time.deltaTime, 0);
			ShotMiniBullets ();
		 }
	}
	void ShotMiniBullets()
	{
		float z = transform.rotation.eulerAngles.z;
		cartridge.GetBullet ().Shot (transform.position, -45 + z);
		cartridge.GetBullet ().Shot (transform.position, 45 + z);
		cartridge.GetBullet ().Shot (transform.position, 0 + z);



		Reset ();
	}
}

	

